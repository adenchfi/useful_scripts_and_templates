(TeX-add-style-hook
 "math_pkgs"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("ntheorem" "thmmarks" "amsmath" "thref") ("inputenc" "utf8") ("babel" "english") ("cleveref" "capitalize")))
   (add-to-list 'LaTeX-verbatim-environments-local "lstlisting")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "lstinline")
   (TeX-run-style-hooks
    "amsmath"
    "amssymb"
    "amsfonts"
    "gensymb"
    "ntheorem"
    "graphicx"
    "hyperref"
    "physics"
    "inputenc"
    "babel"
    "cancel"
    "cleveref"
    "enumitem"
    "listings"
    "caption")
   (TeX-add-symbols
    '("pvec" 1)
    '("evalat" 1)
    '("ov" 1)
    '("EV" 3)
    '("inner" 2)
    '("mean" 1)
    "FT"
    "BB"
    "Ang"
    "spn"
    "N"
    "Z"
    "indep")
   (LaTeX-add-ntheorem-newtheorems
    "theorem"
    "corollary"
    "lemma"
    "definition"
    "example"
    "exercise")
   (LaTeX-add-caption-DeclareCaptions
    '("\\DeclareCaptionFormat{mylst}" "Format" "mylst")))
 :latex)

