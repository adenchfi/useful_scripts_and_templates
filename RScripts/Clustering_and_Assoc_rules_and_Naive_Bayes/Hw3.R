# Homework assignment 3 for CS422, written by Adam Denchfield. 

library("caret")
library("ROCR")
library("e1071")
rm(list=ls())
# Problem 2.1: Naive Bayes classifier compared to decision trees.
setwd("C:/Users/Adam Denchfield/Desktop/CS422")
data <- read.csv("ILPD.csv", header=T, sep=",", comment.char = '#')

set.seed(100)
index <- sample(1:nrow(data), size=0.4*nrow(data))
test <- data[index, ]
train <- data[-index, ]

# Build the model using only two predictor variables.  Explore the
# accuracy of the model using other variables.
model <- naiveBayes(as.factor(label) ~ ., method="class", data=train)
pred <- predict(model, newdata = test)
# More information about the model
#print(model)  # **** See Note 1 below on model interpretation.
#table(test[,11])  # Show real class distribution
#table(pred)       # Show predicted class distribution
#table(pred, test[,11]) # Manual confusion matrix
cM = confusionMatrix(pred, reference = test[, 11])
print(cM)
# high specificity, high PPV, Accuracy = 56% though...

# ROC curve
pred.rocr <- predict(model, newdata=test, type="raw") # Posterior probabilities
f.pred <- prediction(pred.rocr[,2], test$label)
f.perf <- performance(f.pred, "tpr", "fpr")
plot(f.perf, colorize=T, lwd=3)
abline(0,1)
auc <- performance(f.pred, measure = "auc")
auc@y.values[[1]]

# Surprisingly, it looks like the Naive Bayes classifier performs WORSE than the tree-based classifiers
# from HW 2. I was expecting it to do way better! Testing other seeds for splitting the data seems to 
# yield some that produce better results. e.g. seed(102) produced an accuracy of 61%. 

# problem 2.2, Association Rules
library(arules)
trans <- read.transactions("groceries.csv", sep=",")
summary(trans)
# a) What is the most frequently bought item? whole milk, with 2513 appearances. 
# Its frequency is then 2513 / 9835 transactions = 25.5% of the time
# b) What is the least frequently bought item? 
freqs <- sort(itemFrequency(trans), decreasing=FALSE)
freqs[1] # Baby food is the least frequent! Weird, but okay.
freqs[2] # Second least frequent is sound storage medium. 
inspect(trans[1:10])
itemFrequencyPlot(trans, support = 0.1)

# c)
rules <- apriori(trans, parameter = list(supp = 0.255, conf = 0.01)) 
# this is when rules begin to be generated. Supp = 0.255. 
summary(rules)

# Using a low confidence level and a lower support... because, well, no rules were being generated.
rules <- apriori(trans, parameter = list(supp = 0.01, conf = 0.50)) 
inspect(rules)
options(digits = 3) # FINALLY found how to do this... god, the R documentation SUCKS

inspect(sort(rules, decreasing = TRUE, by = "support")) # top 5 by support
# d)
#     lhs                                      rhs                support confidence lift count
#[1]  {other vegetables,yogurt}             => {whole milk}       0.0223  0.513      2.01 219  
#[2]  {tropical fruit,yogurt}               => {whole milk}       0.0151  0.517      2.02 149  
#[3]  {other vegetables,whipped/sour cream} => {whole milk}       0.0146  0.507      1.98 144  
#[4]  {root vegetables,yogurt}              => {whole milk}       0.0145  0.563      2.20 143  
#[5]  {other vegetables,pip fruit}          => {whole milk}       0.0135  0.518      2.03 133  
summary(rules) # It took me way too long to find out how to not have it round to the nearest integer.
# g)
inspect(sort(rules, decreasing = FALSE, by = "support")) # bottom 5 by support
#lhs                                      rhs                support confidence lift count
#[1]  {curd,yogurt}                         => {whole milk}       0.0101  0.582      2.28  99  
#[2]  {citrus fruit,root vegetables}        => {other vegetables} 0.0104  0.586      3.03 102  
#[3]  {whipped/sour cream,yogurt}           => {whole milk}       0.0109  0.525      2.05 107  
#[4]  {butter,other vegetables}             => {whole milk}       0.0115  0.574      2.24 113  
#[5]  {root vegetables,tropical fruit}      => {whole milk}       0.0120  0.570      2.23 118 

# e)
inspect(sort(rules, decreasing = TRUE, by = "confidence")) # top 5 by confidence
# you get 
"lhs                                      rhs                support confidence lift count
[1]  {citrus fruit,root vegetables}        => {other vegetables} 0.0104  0.586      3.03 102  
[2]  {root vegetables,tropical fruit}      => {other vegetables} 0.0123  0.585      3.02 121  
[3]  {curd,yogurt}                         => {whole milk}       0.0101  0.582      2.28  99  
[4]  {butter,other vegetables}             => {whole milk}       0.0115  0.574      2.24 113  
[5]  {root vegetables,tropical fruit}      => {whole milk}       0.0120  0.570      2.23 118 
"
# h) 
inspect(sort(rules, decreasing = FALSE, by = "lift")) # bottom 5 by confidence
"
     lhs                                      rhs                support confidence lift count
[1]  {other vegetables,whipped/sour cream} => {whole milk}       0.0146  0.507      1.98 144  
[2]  {other vegetables,yogurt}             => {whole milk}       0.0223  0.513      2.01 219  
[3]  {tropical fruit,yogurt}               => {whole milk}       0.0151  0.517      2.02 149  
[4]  {other vegetables,pip fruit}          => {whole milk}       0.0135  0.518      2.03 133  
[5]  {rolls/buns,root vegetables}          => {whole milk}       0.0127  0.523      2.05 125  
"

# f)
inspect(sort(rules, decreasing = TRUE, by = "lift")) # top 5 by lift
"     lhs                                      rhs                support confidence lift count
[1]  {citrus fruit,root vegetables}        => {other vegetables} 0.0104  0.586      3.03 102  
[2]  {root vegetables,tropical fruit}      => {other vegetables} 0.0123  0.585      3.02 121  
[3]  {rolls/buns,root vegetables}          => {other vegetables} 0.0122  0.502      2.59 120  
[4]  {root vegetables,yogurt}              => {other vegetables} 0.0129  0.500      2.58 127  
[5]  {curd,yogurt}                         => {whole milk}       0.0101  0.582      2.28  99  
"

# i)
inspect(sort(rules, decreasing = FALSE, by = "lift")) # bottom 5 by lift
"
[1]  {other vegetables,whipped/sour cream} => {whole milk}       0.0146  0.507      1.98 144  
[2]  {other vegetables,yogurt}             => {whole milk}       0.0223  0.513      2.01 219  
[3]  {tropical fruit,yogurt}               => {whole milk}       0.0151  0.517      2.02 149  
[4]  {other vegetables,pip fruit}          => {whole milk}       0.0135  0.518      2.03 133  
[5]  {rolls/buns,root vegetables}          => {whole milk}       0.0127  0.523      2.05 125 
"

# Problem 2.3. Thank god I'm done with arules, the R documentation sucks. 
"
 file19 basically has a bunch of comments, a title line, and 
 then the number values preceding an animal type. 
 I made a Python script that cleans the data for me. That way any file like that (on the HARTIGAN site)
 can be cleaned using the same script. I included it for posterity's sake. It doesn't keep the animal 
 names.
 ii) The data are all digits from 0 to 10. I'm going to say they don't need to be standardized,
  especially considering they're all teeth numbers.
"
library(fpc)
library(factoextra)

teeth_data = read.csv("file19.csv", header=F)
fviz_nbclust(teeth_data, FUNcluster = kmeans, method="silhouette")
# lowest silhouette avg width is with 3 clusters. 
k <- kmeans(teeth_data, centers = 3, nstart = 25) # try 25 times and choose best
fviz_cluster(k, data = teeth_data, main="Clusters") 
# looks odd; there's a couple outliers, data points 54 and 12. 
teeth_data[12, ] # definitely a weird one!
teeth_data[54, ]

# Tried keeping the data names
teeth_data2 = read.csv("file19_2.csv", header=T, sep = ",")
fviz_nbclust(teeth_data2, FUNcluster = kmeans, method="silhouette")
# lowest silhouette avg width is with 3 clusters. 
k2 <- kmeans(teeth_data2, centers = 3, nstart = 25) # try 25 times and choose best
fviz_cluster(k2, data = teeth_data2, main="Clusters") 
# looks odd; there's a couple outliers, data points 54 and 12. 
teeth_data2[12, ] # definitely a weird one!
teeth_data2[54, ]

print(k2)
# 3 clusters with sizes 20, 28, and 18. 
# iv) total SSE = 206
# v) cluster SSEs are 75.1, 91.1, and 40.3 respectively. 
which(k2$cluster == 1)
# these are 
# 
# "Armadillo"       0 0 0 0 0 0 8 8
# "Pika"            2 1 0 0 2 2 3 3
# "Snowshoe rabbit" 2 1 0 0 3 2 3 3
# "Beaver"          1 1 0 0 2 1 3 3
# "Marmot"          1 1 0 0 2 1 3 3
# "Groundhog"       1 1 0 0 2 1 3 3
# "Prairie Dog"     1 1 0 0 2 1 3 3
# "Ground Squirrel" 1 1 0 0 2 1 3 3
# "Chipmunk"        1 1 0 0 2 1 3 3
# "Gray squirrel"   1 1 0 0 1 1 3 3
# "Fox squirrel"    1 1 0 0 1 1 3 3
# "Pocket gopher"   1 1 0 0 1 1 3 3
# "Kangaroo rat"    1 1 0 0 1 1 3 3
# "Pack rat"        1 1 0 0 0 0 3 3
# "Field mouse"     1 1 0 0 0 0 3 3
# "Muskrat"         1 1 0 0 0 0 3 3
# "Black rat"       1 1 0 0 0 0 3 3
# "House mouse"     1 1 0 0 0 0 3 3
# "Porcupine"       1 1 0 0 1 1 3 3
# "Guinea pig"      1 1 0 0 1 1 3 3
# 
# They are all the small mammals. Makes sense. 

which(k2$cluster == 2)
# these are

# "Opossum"         5 4 1 1 3 3 4 4
# "Hairy tail mole" 3 3 1 1 4 4 3 3
# "Common mole"     3 2 1 0 3 3 3 3
# "Star nose mole"  3 3 1 1 4 4 3 3
# "Wolf"            3 3 1 1 4 4 2 3
# "Fox"             3 3 1 1 4 4 2 3
# "Bear"            3 3 1 1 4 4 2 3
# "Civet cat"       3 3 1 1 4 4 2 2
# "Raccoon"         3 3 1 1 4 4 3 2
# "Marten"          3 3 1 1 4 4 1 2
# "Fisher"          3 3 1 1 4 4 1 2
# "Weasel"          3 3 1 1 3 3 1 2
# "Mink"            3 3 1 1 3 3 1 2
# "Ferrer"          3 3 1 1 3 3 1 2
# "Wolverine"       3 3 1 1 4 4 1 2
# "Badger"          3 3 1 1 3 3 1 2
# "Skunk"           3 3 1 1 3 3 1 2
# "River otter"     3 3 1 1 4 3 1 2
# "Sea otter"       3 2 1 1 3 3 1 2
# "Jaguar"          3 3 1 1 3 2 1 1
# "Ocelot"          3 3 1 1 3 2 1 1
# "Cougar"          3 3 1 1 3 2 1 1
# "Lynx"            3 3 1 1 3 2 1 1
# "Fur seal"        3 2 1 1 4 4 1 1
# "Sea lion"        3 2 1 1 4 4 1 1
# "Walrus"          1 0 1 1 3 3 0 0
# "Grey seal"       3 2 1 1 3 3 2 2
# "Elephant seal"   2 1 1 1 4 4 1 1

# I would have expected the moles to be with the first cluster, and the Wolf/Fox to be in the next one.
which(k2$cluster == 3)
# these are

# "Brown bat"       2 3 1 1 3 3 3 3
# "Silver hair bat" 2 3 1 1 2 3 3 3
# "Pigmy bat"       2 3 1 1 2 2 3 3
# "House bat"       2 3 1 1 1 2 3 3
# "Red bat"         1 3 1 1 2 2 3 3
# "Hoary bat"       1 3 1 1 2 2 3 3
# "Lump nose bat"   2 3 1 1 2 3 3 3
# "Coyote"          1 3 1 1 4 4 3 3
# "Peccary"         2 3 1 1 3 3 3 3
# "Elk"             0 4 1 0 3 3 3 3
# "Deer"            0 4 0 0 3 3 3 3
# "Moose"           0 4 0 0 3 3 3 3
# "Reindeer"        0 4 1 0 3 3 3 3
# "Antelope"        0 4 0 0 3 3 3 3
# "Bison"           0 4 0 0 3 3 3 3
# "Mountain goat"   0 4 0 0 3 3 3 3
# "Musk ox"         0 4 0 0 3 3 3 3
# "Mountain sheep"  0 4 0 0 3 3 3 3

# weird to think bats were paired with things like antelopes. I figured Wolf/Fox would be in here.

