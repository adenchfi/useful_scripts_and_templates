# Script written by Adam Denchfield. This takes files, skips comment rows indicated with '#', and reorganizes the data.
# It ignores non-numeric strings. Basically, a datafile like:
'''

# "Name"           "I" "i" "C" "c" "P" "p" "M" "m"
"Opossum"         5 4 1 1 3 3 4 4
"Hairy tail mole" 3 3 1 1 4 4 3 3

'''

# will turn into an output text file styled like a csv, WITHOUT the words
# at the beginning

import sys

def skip_comments(file):
    for line in file:
        if not line.strip().startswith('#'):
            yield line

lines_to_write = [] # empty list
filename = sys.argv[1]
idx = 0
with open(filename) as f:
    for line in skip_comments(f):
        newline = ""
        if (idx == 0): # accepts first non-comment line as the titles
            items = line.split() # take out first item
            for item in items[0:len(items)]:
                if (item != items[-1]):
                    newline += str(item) + ','
                    print(item)
                elif (item == items[-1]):
                    newline += str(item)
            lines_to_write.append(newline)
            newline = ""
            idx += 1
            continue
        items = line.split() # hopefully I don't keep the \n
        for idx, item in enumerate(items):
            if (idx != (len(items) - 1)):
                if ('0' <= item[0] <=  '9'):
                    item = str(item) + ","
                else:
                    continue
                
                # characters not digits should be remapped
                #        line += str(item[0]) + ','
                #        if (item != items[-1]): # not the last item
                #            item = str(item) + ","         # add a comma after each word
            newline += str(item)
        lines_to_write.append(newline)
with open('output.csv', 'w') as o:
    for line in lines_to_write:
        if '\n' in line:
            o.write(line)
        else: # for my purposes, this covers everything else. Change this if re-using 
            o.write(line + '\n')
