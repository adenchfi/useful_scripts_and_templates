# Written by Adam Denchfield, for CS422 Data Mining
from numpy import arange

def S_curve_eval(s, r, b):
    return (1 - (1 - s**r)**b)

s_vals = arange(0.1, 1.0, 0.1)
# first bullet for the homework
r1 = 3
b1 = 10
s1 = S_curve_eval(s_vals, r1, b1)
print(s1)

r2 = 6
b2 = 20
s2 = S_curve_eval(s_vals, r2, b2)
print(s2)
