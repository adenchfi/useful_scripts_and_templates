# Written by Adam Denchfield, for CS422 Data Mining
from numpy import arange
import matplotlib.pyplot as plt
def B_curve_eval(s, r_tot, b):
    return (1 - 0.888 - (1 - s**(r_tot/b))**b)

b_vals = arange(1, 100, 1)
# first bullet for the homework
r1 = 240
s1 = 0.3
b1 = B_curve_eval(s1, r1, b_vals)
print(b1)
plt.plot(b_vals, b1)
plt.axhline(y=0)
plt.axvline(x=80)
plt.xticks(arange(0, 100, 5))
plt.title("Graph of b_values versus cand. pair prob. minus 0.888")
plt.xlabel("Band number")
plt.ylabel("Probability minus 0.888")
plt.show()
#r2 = 6
#b2 = 20
#s2 = S_curve_eval(s_vals, r2, b2)
#print(s2)
