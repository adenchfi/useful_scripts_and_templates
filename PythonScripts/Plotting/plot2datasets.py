import numpy as np
import matplotlib.pyplot as plt
import sys

x1,y1=np.genfromtxt(sys.argv[1],dtype=float,unpack=True)
x2,y2=np.genfromtxt(sys.argv[2],dtype=float,unpack=True)

#xf = np.concatenate(x1,x2)
#yf = x2 * 0
#yf = np.concatenate(yf, y1)
#y1min = y1 * 0

# plt.vlines(x1, y1min, y1, color='b', label='')
label1, = plt.plot(x1,y1, color='b',label=sys.argv[1], linewidth=0.5)#drawstyle='steps'
label2, = plt.plot(x2,y2,'r',label=sys.argv[2],linewidth=1.0, linestyle='dashed')
plt.xticks(np.arange(0, 300, 20.0))
#plt.legend([label1, label2], loc='upper left')
plt.legend(handles=[label1, label2], loc='upper left')
plt.xlabel('Temperature, K')
plt.ylabel('Resistance, Ohms')
plt.show()
