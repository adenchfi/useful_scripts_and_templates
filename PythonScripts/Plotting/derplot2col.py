#%matplotlib inline
from matplotlib import pyplot as plt
import numpy as np
from scipy.interpolate import splrep, splev
import sys

x = [[] for i in range(len(sys.argv))]
y = [[] for j in range(len(sys.argv))]
labels = [[] for l in range(len(sys.argv))]
#labels[len(sys.argv)] = sys.argv[len(sys.argv)]
for args in range(0,len(sys.argv) - 1):
    x[args],y[args] = np.genfromtxt(sys.argv[args + 1], dtype=float, unpack=True)

    spacing = 30
    data = np.diff(y[args][1::spacing])/np.diff(x[args][1::spacing])
    #print(data[round(0.75*len(x[args]))])
    print(len(x[args]))
    fig, ax1 = plt.subplots()
    ax2 = ax1.twinx()

    #f = splrep(x[args],noisy_data,k=5,s=3)
    ax1.plot(x[args], y[args], 'g-', label="raw data")
    x[args] = np.delete(x[args], -1)
    ax2.plot(np.delete(x[args][1::spacing], -1), data, 'b-', label = "1st derivative")
    ax1.set_xlabel('Temperature', size='x-large')
    ax1.set_ylabel('Resistance', color='g', size='x-large')
    ax2.set_ylabel('Derivative of Resistance', color='b', size='x-large')
        
    plt.legend(loc='lower right')
    plt.show()
