import numpy as np
import matplotlib.pyplot as plt
import sys
#x = []
#y = []
x = [[] for i in range(len(sys.argv))]
y = [[] for j in range(len(sys.argv))]
ymax = [[] for k in range(len(sys.argv))]
labels = [[] for l in range(len(sys.argv))]
#labels[len(sys.argv)] = sys.argv[len(sys.argv)]
for args in range(0,len(sys.argv) - 1):
    x[args],y[args] = np.genfromtxt(sys.argv[args + 1], dtype=float, unpack=True)
    ymax[args] = max(y[args])
    #y[args] /= ymax[args]
    plt.plot(x[args], y[args])
    labels[args] = sys.argv[args + 1]
    
plt.legend(labels, fontsize='large', loc='upper left')
plt.xticks(np.arange(0, max(x[0]), 10), size='large')
plt.yticks(size='x-large')
#plt.tick_params(labelsize='x-large')
# plt.vlines(x1, y1min, y1, color='b', label='')

#label1, = plt.plot(x1,y1, color='b',label='TheoreticalRaman')#drawstyle='steps'
#label2, = plt.plot(x2,y2,'r',label='MoN_Si_785nm',linewidth=1.0)
#plt.xticks(np.arange(100,900, 40.0))
#plt.legend(['MoN_Si_785nm', 'TheoreticalRaman',])
#plt.legend(handles=[label1, label2])
plt.xlabel('Temperature, K', fontsize='x-large')
plt.ylabel('Resistance, Ohms', fontsize='x-large')
plt.show()
