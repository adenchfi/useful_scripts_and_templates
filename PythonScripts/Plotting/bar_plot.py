import matplotlib.pyplot as plt
import numpy as np



majors = [252,76,7,7,5,4,3,2,2,1,1,1,1]
x = np.arange(len(majors))

#def millions(x, pos):
#        'The two args are the value and tick position'
#            return '$%1.1fM' % (x * 1e-6)


#formatter = FuncFormatter(millions)

fig, ax = plt.subplots()
#ax.yaxis.set_major_formatter(formatter)
plt.bar(x, majors, align='center')
names = ('ITM','INTO','INTM','FS & Tech', 'Data An.','CF & Sec.','Data Sci','CS Tech.','CIS','M. Anal.','App. Anal.', 'App. Tech.','MEM')
plt.xticks(x, names)
for i, v in enumerate(majors):
    ax.text(i-.10, v+1, str(v), color='blue', fontweight='bold')

plt.title("Fall 2017 School of Applied Technology Attendance, by Major")
plt.show()
