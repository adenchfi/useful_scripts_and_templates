import numpy as np
from numpy import shape
from numpy import mean
import scipy
from scipy.sparse import diags
from scipy.sparse.linalg import eigsh
import matplotlib.pyplot as plt
from scipy.constants import *

m = 10e-31 # mass of an electron, approximately
a = 3.567e-10 # diamond as an example
L = 100*a # consider a domain that's 100 lattice constants long
V = -3.0e-19 # around an eV
ns = 5   # number of different G-vectors to consider
g = [n*pi/a for n in range(0, ns)]
gs = [gi - mean(g) for gi in g] # will be symmetric about g = 0

def lam(k):
    return (hbar * k)**2 / (2*m)

Vs = [V for i in range(len(g) - 1)]     # same potential
eigs = [[] for i in range(len(g) - 1)]
ks = np.arange(-pi/a, pi/a, 2*pi/L)

for k in ks:
    kgs = [k + gi for gi in gs] # will be symmetric around k = pi/a

    lams = [lam(k) for k in kgs]
    M = diags((Vs, lams, Vs), (-1, 0, 1))   # construct a tridiagonal matrix with the potentials in the off-diagonal parts. Only tridiagonal because of cosine potential and the fact that cos(-pi*x) = cos(pi*x).
    Mnp = np.matrix(M)
    tmp_eigvals, vecs = eigsh(M, k = 4) # get the eigenvalues, eigenvectors
    for num, which_egnval in enumerate(eigs):  # re-order the solution
        which_egnval.append(tmp_eigvals[-num]) # last element is largest eigenvalue

for eig in eigs:
    plt.plot(ks, eig) # x, y
plt.title("Band Structure Using Central Equation, Potential $V = -2.0eV$, $a = 3.567$ Ang.\n Using a sample periodic potential $V = \cos (n\pi/a) x$", fontsize=18)
plt.show()
