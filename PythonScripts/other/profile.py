#!/usr/bin/env python
from __future__ import print_function
import os
import shutil
import argparse

# profiling settings
probtype = 'test_uncon'
flags = ''
solver = 'bncg'
outdir = 'bncg'

# configure the parser
parser = argparse.ArgumentParser()
parser.add_argument('-p', '--probtype', type=str, nargs=1, default=[probtype], help='specify the CUTEst problem set to be tested')
parser.add_argument('-f', '--flags', type=str, nargs=1, default=[flags], help='specify the flags to run the solver with')
parser.add_argument('-s', '--solver', type=str, nargs=1, default=[solver], help='specify the TAO algorithm to be tested')
parser.add_argument('-a', '--all', default=False, action="store_true", help='force all problems (ignore excludes)')
parser.add_argument('-e', '--excludes', default=False, action="store_true", help='run excluded problems')
parser.add_argument('-o', '--outdir', type=str, nargs=1, default=[outdir], help='specify the directory for output files')
args = parser.parse_args()
probtype = args.probtype[0]
flags = args.flags[0]
outdir = args.outdir[0]
solver = args.solver[0]
run_all = args.all
run_excludes = args.excludes

# prepare the list of CUTEst problems to be evaluated
fname = '%s/probs.txt'%probtype
with open(fname) as f:
    problems = f.readlines()
problems = [p.strip() for p in problems]
fname = '%s/excludes.txt'%probtype
with open(fname) as f:
    excludes = f.readlines()
excludes = [p.strip() for p in excludes]

# prepare the output directory
fulldir = './%s/%s'%(probtype, outdir)

# prepare the petsc options
opts = 'PETSC_OPTIONS=\"-tao_type %s -tao_gmonitor -tao_view -tao_max_it 1000 -tao_max_funcs 10000 %s\"'%(solver, flags)

# run the test set
print('testing %s with opts: %s'%(solver, flags))

if run_all:
    final_set = problems
elif run_excludes:
    final_set = excludes
else:
    final_set = [p for p in problems if p not in excludes]
    
if not os.path.exists(fulldir):
    os.makedirs(fulldir)
    
# loop over problems for the solver
for pidx, p in enumerate(final_set):
    print('  %s'%p)
    outname = '%s/%s.out'%(fulldir,p)
    if os.path.exists(outname):
        os.remove(outname)
    os.system('%s $CUTEST/bin/runcutest --limit 10 -k -r -p tao -D %s > %s'%(opts,p,outname))
    os.system('mkdir %s/run_tao_%s'%(fulldir, p))
    os.system('mv ./run_tao %s/run_tao_%s'%(fulldir,p))
    os.system('mv *.d %s/run_tao_%s/'%(fulldir, p))
    # clean up leftover files
    os.system('rm *.f')
    #os.system('rm *.d')
    #os.system('rm -rf run_tao.dSYM')
    
