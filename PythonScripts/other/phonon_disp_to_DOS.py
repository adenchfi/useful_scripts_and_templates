from math import pi
w = 2*pi*v # v is the phonon frequency in Hz
hbar = 1.05457e-34
>>> hbar * w
"""
Get your 3D phonon curves in your brillouin zone. You'd go along a direction, choose a particular k point, get all the points at which the energy/omega is constant, get the sum and divide by the group velocity. Like """
5.3008469835139005e-21
>>> hbar * w / 1.6e-19
0.03313029364696188
>>> 0.008 * 1.6e-19 / hbar
12137648520249.959
>>> 0.004 * 1.6e-19 / hbar
6068824260124.9795
>>> 6e12/(2*pi)
954929658551.3721
