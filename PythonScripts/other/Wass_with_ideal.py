#!/usr/bin/env python

from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
import collections

import numpy as np
import numpy.linalg as la
import scipy.stats as stats
import scipy.interpolate as interp
import matplotlib.pyplot as plt
import argparse

#def stepfit(x, y):
    
def get_prev_x(vals, x):
    for idx, val in enumerate(vals):
        if idx >= x:
            return idx-1
    else:
        return idx

def expand_pmf(ydata, xdata, xs):
    y_new = ydata
    #print(np.shape(y_new))
    for x in xs:
        if x in xdata:
            continue
        else:
            # assumes the provided xs are in sorted order
#            print("inserting...")
            y_new = np.insert(y_new, get_prev_x(ydata, x)+1, 0.0)
#    print(np.shape(y_new))
    return y_new
# problem type
probtype = 'bound'
suffix = 'qn'

# configure the parser
parser = argparse.ArgumentParser()
parser.add_argument('-p', '--probtype', type=str, nargs=1, default=[probtype], help='specify the CUTEst problem set')
parser.add_argument('-s', '--suffix', type=str, nargs=1, default=[suffix], help='specify the suffix for the comparison files to be plotted')
args = parser.parse_args()
probtype = args.probtype[0]
suffix = args.suffix[0]

# read the TAO solvers from the header comment of the data file
with open('%s/iters_%s.out'%(probtype,suffix)) as f:
    raw_iters = f.readlines()
solvers = raw_iters[0]
solvers = solvers.strip()
solvers = solvers.split(' ')
solvers = solvers[2:]
print(solvers)

# load the data files and get the sizes
iters = np.loadtxt('%s/iters_%s.out'%(probtype,suffix))
fevals = np.loadtxt('%s/fevals_%s.out'%(probtype,suffix))
kspits = np.loadtxt('%s/kspits_%s.out'%(probtype,suffix))
cputime = np.loadtxt('%s/cputime_%s.out'%(probtype,suffix))
nprob, nsolver = np.shape(iters)
nsolver = len(solvers)

# generate empty sets to store ratios
iters_ratio = np.zeros((nprob, nsolver))
fevals_ratio = np.zeros((nprob, nsolver))
kspits_ratio = np.zeros((nprob, nsolver))
cputime_ratio = np.zeros((nprob, nsolver))

# loop over problems and compute ratios
eps = np.power(np.finfo(float).eps, old_div(2.,3.))
for i in range(nprob):

    iters_ratio[i,:] = np.divide(np.maximum(iters[i,:], np.ones(nsolver, dtype=int)),np.nanmax([np.nanmin(iters[i,:]), 1]))

    fevals_ratio[i,:] = np.divide(np.maximum(fevals[i,:], np.ones(nsolver, dtype=int)),np.nanmax([np.nanmin(fevals[i,:]), 1]))

    kspits_ratio[i,:] = np.divide(np.maximum(kspits[i,:], np.ones(nsolver, dtype=int)),np.nanmax([np.nanmin(kspits[i,:]), 1]))

    iters_ratio[i,:] = np.divide(np.maximum(iters[i,:], np.ones(nsolver, dtype=int)),np.nanmax([np.nanmin(iters[i,:]), 1]))
    
    cputime_ratio[i,:] = np.divide(np.maximum(cputime[i,:], eps*np.ones(nsolver)),np.nanmax([np.nanmin(cputime[i,:]), eps]))

# identify failed problem and set their cost to twice the maximum
max_iters = np.nanmax(iters_ratio.flatten())
max_evals = np.nanmax(fevals_ratio.flatten())
max_ksp = np.nanmax(kspits_ratio.flatten())
max_time = np.nanmax(cputime_ratio.flatten())

iters_ratio[np.isnan(iters_ratio)] = max_iters
fevals_ratio[np.isnan(fevals_ratio)] = max_evals
kspits_ratio[np.isnan(kspits_ratio)] = max_ksp
cputime_ratio[np.isnan(cputime_ratio)] = max_time

iters_ratio = np.sort(iters_ratio, axis=0)
fevals_ratio = np.sort(fevals_ratio, axis=0)
kspits_ratio = np.sort(kspits_ratio, axis=0)
cputime_ratio = np.sort(cputime_ratio, axis=0)

iters_hists = [[] for i in range(nsolver)] 
feval_hists = [[] for i in range(nsolver)] 

all_it_xs = np.unique(iters_ratio.flatten())
"""
iter_count = []
iter_x = []
iter_y = []
for idx in range(nsolver):
    iter_count.append(collections.Counter(iters_ratio[:, idx]))
    iter_x.append(iter_count.items())
"""
plt.figure(0)
hfig, hax = plt.subplots(nrows=2, ncols=2, figsize=(12, 12))
hfig.subplots_adjust(hspace=0.5, wspace=0.5)
hax[0, 0].set_xscale('log')
hax[1, 0].set_xscale('log')
hax[0, 1].set_xscale('log')
hax[1, 1].set_xscale('log')

pmfs = []
joint_pmfs = [] # expecting to make them joint between iters_ratio and fevals_ratio for now 

for i in range(nsolver):
    iter_bins = np.unique(iters_ratio[:, i])
    feval_bins = np.unique(fevals_ratio[:, i])
    iters_hists[i] = hax[0, 0].hist(iters_ratio[:, i], bins=iter_bins, label=solvers[i], cumulative=True, histtype='step', density=True)
    feval_hists[i] = hax[1, 0].hist(fevals_ratio[:, i], bins=np.unique(fevals_ratio[:, i]), label=solvers[i], cumulative=True, histtype='step', density=True)
    cdf_it = iters_hists[i][0]  
    d_it = iters_hists[i][1] # bin ranges
    
    pmf_it = hax[0, 1].hist(iters_ratio[:, i], bins=iter_bins, label=solvers[i], histtype='step', density=True, weights=np.ones(len(iters_ratio[:, i])) / len(iters_ratio[:, i]))
    # could try using np.clip to make the last stuff prettier, but oh well!
    #print(min(pmf_it[0]))
    #y_pmf_it, x_pmf_it = pmf_it[0], pmf_it[1]
    pmfs.append(np.array(pmf_it[0]))
    
    joint_pmfs = np.histogramdd([iters_ratio[:, i], fevals_ratio[:, i]], bins=[iter_bins, feval_bins])
    
#hax[0, 0].legend(loc='lower right')
hax[0, 0].set_title("Iteration Ratios CDF")
hax[0, 1].set_title("Iteration Ratios PDF")
#hax[0, 0].set_xlim([1, 100])
#hax[0, 1].set_xlim([1, 100])
#hax[1, 0].legend(loc='lower right')
#hax[1, 1].legend(loc='lower right')
#hax[1, 0].set_title("Feval Ratios CDF")
#hax[1, 1].set_title("CDF gotten from xvals_to_cdf")
#hax[0, 1].set_title("Hopefully fit PDFs...")
#hfig.savefig('%s/iters_hist_%s.png'%(probtype, suffix))
#hfig.savefig('%s/fevals_hist_%s.png'%(probtype, suffix))
hfig.savefig('%s/both_hist_%s.png'%(probtype, suffix))

wait = input("Histogram plot saved... Calculate KL divergence matrix of Iteration_ratio pdfs with respect to each other?")

# need to make sure the pdfs are the same shape/size
#maxpdf_size = max([len(p) for p in pdf])
    
H_mat = np.zeros((nsolver+1, nsolver+1))
#entrop_mat = np.zeros((nsolver, 1))
# just need the x-values to feed into the fits
# choose them to be all the unique x-values in iters_ratio
best_solver = ''

# could generate a joint PDF of all metrics and then compare, instead of this
xs = np.unique(iters_ratio.flatten())

iter_ideal = np.zeros(len(xs) - 1)
print(len(xs))
iter_ideal[0] = 0.95 # one spike at one, then nothing after
iter_ideal[1] = 0.04
iter_ideal[2] = 0.01
pmfs.append(iter_ideal)
solvers.append("Ideal")

pmfs = np.array(pmfs)

print(len(pmfs[0]), len(pmfs[1]), len(pmfs[2]))

for i in range(nsolver+1):
    #print(np.shape(pmfs[i]))
    if i != nsolver:
        pmf_i = np.array(expand_pmf(pmfs[i], iters_ratio[:, i], xs))
    else:
        pmf_i = np.array(pmfs[i])
    #print(np.shape(pmf_i))

    #print(pmf_i)
    #entrop_mat[i] = stats.entropy(pmf_i)    
    #print(solvers[i], pmf_i)
    for j in range(nsolver+1):
        if j != nsolver:
            pmf_j = np.array(expand_pmf(pmfs[j], iters_ratio[:, j], xs))
        else:
            pmf_j = np.array(pmfs[j])
        #print(stats.entropy(pmf_i, pmf_j))
        #print(np.shape(pmf_i), np.shape(pmf_j))
        H_mat[i, j] = 0.5*(stats.energy_distance(pmf_i, pmf_j) + stats.energy_distance(pmf_j, pmf_i))
        #print(H_mat[i, j], solvers[i], solvers[j])

#print(H_mat[:, j], solvers)
#best_solver = solvers[np.argmax(entrop_mat)]
#print("Best solver: ", best_solver)
#print(entrop_mat)
# don't even THINK about touching the below parts, except to change the cmap
fig, ax1 = plt.subplots(1, 1, figsize=(13,13))
im = ax1.imshow(H_mat, cmap='Greys', interpolation='nearest', origin='upper', extent=[0, nsolver+1, 0, nsolver+1])
fig.colorbar(im)

for idx in range(nsolver+1):
    ax1.axvline(idx, color='black', ls='--')
    ax1.axhline(idx, color='black', ls='--')
ax1.set_xticks(np.arange(0.5, nsolver+1.5, 1.0))
#ax1.set_xlabel("Denominator")
#ax1.set_ylabel("Numerator")
ax1.set_yticks(np.arange(0.5, nsolver+1.5, 1.0))
ax1.set_title("Darker means farther distance with Wasserstein metric")


for idx, solver in enumerate(solvers):
    #solvers[idx] = solvers[idx][2:8]
    solvers[idx] = solvers[idx].replace("/", "\n")

ax1.set_xticklabels(solvers)
ax1.set_yticklabels(solvers)


#plt.xticks(range(nsolver), labels=solvers)
#plt.yticks(, labels=solvers)
plt.savefig('%s/iters_wass_map_%s.png'%(probtype, suffix), title="Color map of Wass distances between functions")
# above is working fine!

#for i in range(nsolver)


