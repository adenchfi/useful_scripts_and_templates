# the point of this script is to compare the outputted vectors in a test problem, GULF, for PETSc/tao, different solvers.
# Want to take their difference and plot the norm of their difference vs iteration count. 

import sys
import numpy as np
import matplotlib.pyplot as plt
from numpy.linalg import norm
fname1 = sys.argv[1]
fname2 = sys.argv[2]

"""
with open(fname1) as f:
    content1 = f.readlines()

with open(fname2) as f:
    content2 = f.readlines()
"""
lines1 = [line.rstrip('\n') for line in open(fname1)]
lines2 = [line.rstrip('\n') for line in open(fname2)]
vecs1 = [] # append vectors here
vecs2 = []

for idx in range(len(lines1)):
    if "Vec Object" in lines1[idx]:
        v1 = float(lines1[idx+3])
        v2 = float(lines1[idx+4])
        v3 = float(lines1[idx+5])
        vecs1.append([v1, v2, v3])



for idx in range(len(lines2)):
    if "Vec Object" in lines2[idx]:
        v1 = float(lines2[idx+3])
        v2 = float(lines2[idx+4])
        v3 = float(lines2[idx+5])
        vecs2.append([v1, v2, v3])
    
vecnorms = []
avgnorms = []
for idx in range(min(len(vecs1), len(vecs2))):
    vdiff = np.subtract(vecs1[idx], vecs2[idx])
    avgnorms.append(np.mean([norm(vecs1[idx]), norm(vecs2[idx])]))
    #vdiff *= idx
    vecnorms.append(norm(vdiff))
    vecnorms[idx] vecnorms[0]
#vecnorms = np.divide(vecnorms, avgnorms)    
#print(vecs1)
#print(vecs2)
#plt.figure(1)
plt.plot(vecnorms)
plt.savefig("norm_error_vs_iter.png")
