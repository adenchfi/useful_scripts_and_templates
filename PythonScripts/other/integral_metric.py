#!/usr/bin/env python

from __future__ import division
from __future__ import print_function
from builtins import range
from past.utils import old_div
import collections

import numpy as np
import numpy.linalg as la
import scipy.stats as stats
import scipy.interpolate as interp
import matplotlib.pyplot as plt
import argparse

from integral_sieve import *
#def stepfit(x, y):
    
# problem type
probtype = 'bound'
suffix = 'qn'

# configure the parser
parser = argparse.ArgumentParser()
parser.add_argument('-p', '--probtype', type=str, nargs=1, default=[probtype], help='specify the CUTEst problem set')
parser.add_argument('-s', '--suffix', type=str, nargs=1, default=[suffix], help='specify the suffix for the comparison files to be plotted')
args = parser.parse_args()
probtype = args.probtype[0]
suffix = args.suffix[0]

# read the TAO solvers from the header comment of the data file
with open('%s/iters_%s.out'%(probtype,suffix)) as f:
    raw_iters = f.readlines()
solvers = raw_iters[0]
solvers = solvers.strip()
solvers = solvers.split(' ')
solvers = solvers[2:]
#print(solvers)

# load the data files and get the sizes
iters = np.loadtxt('%s/iters_%s.out'%(probtype,suffix))
fevals = np.loadtxt('%s/fevals_%s.out'%(probtype,suffix))
kspits = np.loadtxt('%s/kspits_%s.out'%(probtype,suffix))
cputime = np.loadtxt('%s/cputime_%s.out'%(probtype,suffix))
nprob, nsolver = np.shape(iters)
nsolver = len(solvers)

# generate empty sets to store ratios
iters_ratio = np.zeros((nprob, nsolver))
fevals_ratio = np.zeros((nprob, nsolver))
kspits_ratio = np.zeros((nprob, nsolver))
cputime_ratio = np.zeros((nprob, nsolver))

# loop over problems and compute ratios
eps = np.power(np.finfo(float).eps, old_div(2.,3.))
for i in range(nprob):

    iters_ratio[i,:] = np.divide(np.maximum(iters[i,:], np.ones(nsolver, dtype=int)),np.nanmax([np.nanmin(iters[i,:]), 1]))

    fevals_ratio[i,:] = np.divide(np.maximum(fevals[i,:], np.ones(nsolver, dtype=int)),np.nanmax([np.nanmin(fevals[i,:]), 1]))

    kspits_ratio[i,:] = np.divide(np.maximum(kspits[i,:], np.ones(nsolver, dtype=int)),np.nanmax([np.nanmin(kspits[i,:]), 1]))

    iters_ratio[i,:] = np.divide(np.maximum(iters[i,:], np.ones(nsolver, dtype=int)),np.nanmax([np.nanmin(iters[i,:]), 1]))
    
    cputime_ratio[i,:] = np.divide(np.maximum(cputime[i,:], eps*np.ones(nsolver)),np.nanmax([np.nanmin(cputime[i,:]), eps]))

# identify failed problem and remove those entries
max_iters = np.nanmax(iters_ratio.flatten())
max_evals = np.nanmax(fevals_ratio.flatten())
max_ksp = np.nanmax(kspits_ratio.flatten())
max_time = np.nanmax(cputime_ratio.flatten())


iters_ratio[np.isnan(iters_ratio)] = 2*max_iters
fevals_ratio[np.isnan(fevals_ratio)] = 2*max_evals
kspits_ratio[np.isnan(kspits_ratio)] = 2*max_ksp
cputime_ratio[np.isnan(cputime_ratio)] = 2*max_time

iters_ratio = np.sort(iters_ratio, axis=0)
fevals_ratio = np.sort(fevals_ratio, axis=0)
kspits_ratio = np.sort(kspits_ratio, axis=0)
cputime_ratio = np.sort(cputime_ratio, axis=0)

iters_hists = [[] for i in range(nsolver)] 
feval_hists = [[] for i in range(nsolver)] 

all_it_xs = np.unique(iters_ratio.flatten())
"""
iter_count = []
iter_x = []
iter_y = []
for idx in range(nsolver):
    iter_count.append(collections.Counter(iters_ratio[:, idx]))
    iter_x.append(iter_count.items())
"""
plt.figure(0)
hfig, hax = plt.subplots(nrows=2, ncols=2, figsize=(12, 12))
hfig.subplots_adjust(hspace=0.5, wspace=0.5)
hax[0, 0].set_xscale('log')
hax[1, 0].set_xscale('log')
hax[0, 1].set_xscale('log')
hax[1, 1].set_xscale('log')

cdfs = []
cdf_xs = []
for i in range(nsolver):
    iter_bins = np.unique(iters_ratio[:, i])
    feval_bins = np.unique(fevals_ratio[:, i])
    iters_hists[i] = hax[0, 0].hist(iters_ratio[:, i], bins=iter_bins, label=solvers[i], cumulative=True, histtype='step', density=True)
    feval_hists[i] = hax[1, 0].hist(fevals_ratio[:, i], bins=np.unique(fevals_ratio[:, i]), label=solvers[i], cumulative=True, histtype='step', density=True)
    cdf_it = iters_hists[i][0][0:-1] # exclude the '1'
    cdf_it = np.append(cdf_it, cdf_it[-1])

    #cdf_it.append(cdf_it[-1])
    d_it = iters_hists[i][1] # bin ranges
    cdfs.append(cdf_it) # 
    cdf_xs.append(d_it[0:-1])

    #cdf_it = hax[0, 1].hist(iters_ratio[:, i], bins=iter_bins, label=solvers[i], histtype='step', density=False, weights=np.ones(len(iters_ratio[:, i])) / len(iters_ratio[:, i]))
    # could try using np.clip to make the last stuff prettier, but oh well!
    
    #y_cdf_it, x_cdf_it = cdf_it[0], cdf_it[1]
    #cdfs.append(cdf_it[0])
    

    
#hax[0, 0].legend(loc='lower right')
hax[0, 0].set_title("Iteration Ratios CDF")
#hax[0, 1].set_title("Iteration Ratios PDF")
#hax[0, 0].set_xlim([1, 100])
#hax[0, 1].set_xlim([1, 100])
#hax[1, 0].legend(loc='lower right')
#hax[1, 1].legend(loc='lower right')
hax[1, 0].set_title("Feval Ratios CDF")
hax[1, 1].set_title("CDF gotten from transformation")
#hax[0, 1].set_title("Hopefully fit PDFs...")
#hfig.savefig('%s/iters_hist_%s.png'%(probtype, suffix))
#hfig.savefig('%s/fevals_hist_%s.png'%(probtype, suffix))




# need to make sure the pdfs are the same shape/size
#maxpdf_size = max([len(p) for p in pdf])
    
int_mat = np.zeros((nsolver, nsolver))

# just need the x-values to feed into the fits
# choose them to be all the unique x-values in iters_ratio

flat_xs = [item for sublist in cdf_xs for item in sublist]

xvals = np.unique(flat_xs)

for i in range(nsolver):
    hax[0, 1].step(cdf_xs[i], cdfs[i])
#    cdfs[i] = expand_cdf(cdfs[i], cdf_xs[i], xvals)


hfig.savefig('%s/hist_iter_fevals_%s.png'%(probtype, suffix))
wait = input("Histogram plots saved..." )
# now to conduct the sieve for choosing the best performance plots
#print(solvers)
best_method = sieve(nsolver, solvers, cdf_xs, cdfs, xvals)

