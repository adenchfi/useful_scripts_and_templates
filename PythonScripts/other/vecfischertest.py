#!/usr/bin/env python2

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.patches as patches
import numpy as np
import numbers

# 2D Rosenbrock function and its gradient
a = 1.
b = 100.

def f(x, y):
    return ((a - x)**2. + b*(y - x**2.)**2.)
    
def g(x, y):
    return np.array([-2.*(a - x) - 4.*b*(y - x**2.)*x, 2.*b*(y - x**2.)])
 
# Python version of PETSc's Fischer-Braumeister function (seems to be a bit different than the textbook version)
def FischerPy(u, w):
    assert isinstance(u, numbers.Number) and isinstance(w, numbers.Number)
    if (u + w <= 0):
        return np.sqrt(u*u + w*w) - (u + w)
    else:
        return -2.*u*w / (np.sqrt(u*u + w*w) + (u + w))

# Python version of PETSc VecMedian
def VecMedianPy(l, x, u):
    for i in range(len(x)):
        if l[i] == u[i]:
            x[i] = l[i]
        elif x[i] < l[i]:
            x[i] = l[i]
        elif x[i] > u[i]:
            x[i] = u[i]
    return x

# Python version of PETSc VecFischer
def VecFischerPy(x, f, l, u):
    fb = np.zeros_like(f)
    for i in range(len(x)):
        assert l[i] <= u[i]
        if np.isneginf(l[i]) and np.isinf(u[i]):
            fb[i] = -f[i]
        elif np.isneginf(l[i]):
            fb[i] = -FischerPy(u[i]-x[i], -f[i])
        elif np.isinf(u[i]):
            fb[i] = FischerPy(x[i]-l[i], f[i])
        elif l[i] == u[i]:
            fb[i] = l[i] - x[i]
        else:
            fb[i] = FischerPy(x[i]-l[i], FischerPy(u[i]-x[i], -f[i]))
    return fb
        
# define the domain for the problem
domxl = -1.3
domxu = 1.3
domyl = -0.9
domyu = 1.7
xn = 31
yn = 31
xx = np.linspace(domxl, domxu, xn)
yy = np.linspace(domyl, domyu, yn)
xx, yy = np.meshgrid(xx, yy)
F = np.zeros((yn, xn))
G = np.zeros((yn, xn))

# define the bounds for the problem
lb = np.array([-1., -0.5])
ub = np.array([-0.5, 1.5])
xxb = np.zeros_like(xx)
yyb = np.zeros_like(yy)
pGb = np.zeros_like(G)

fmin = 1e10
minpt = np.zeros_like(lb)
ming = np.zeros_like(lb)
for i in range(yn):
    for j in range(xn):
        # create the local point
        xpt = np.array([xx[i, j], yy[i, j]])
        # evaluate the gradient at that point
        gpt = g(xpt[0], xpt[1])
        # evaluate the function value at that point
        F[i, j] = f(xpt[0], xpt[1])
        # calculate the norm of the gradient
        G[i, j] = np.linalg.norm(gpt)
        # snap the point into the bounds
        pxpt = VecMedianPy(lb, xpt, ub)
        xxb[i, j] = pxpt[0]
        yyb[i, j] = pxpt[1]
        # recompute the gradient at the bound snapped point
        pgpt = g(pxpt[0], pxpt[1])
        # run VecFischer on the bounded gradient
        fbgpt = VecFischerPy(xpt, gpt, lb, ub)
        # compute the norm of the VecFischer gradient
        pGb[i, j] = np.linalg.norm(fbgpt)
        # search for the minimum
        if F[i, j] < fmin and lb[0] <= xpt[0] <= ub[0] and lb[1] <= xpt[1] <= ub[0]:
            fmin = F[i, j]
            minpt = xpt
            ming = gpt
            mingnorm = G[i, j]
            minfbg = fbgpt
            minfbgnorm = pGb[i, j]
        
# start plotting everything
f, axarr = plt.subplots(3, sharex=True)
rect1 = patches.Rectangle((lb[0],lb[1]),ub[0]-lb[0],ub[1]-lb[1],linewidth=1,edgecolor='r',facecolor='none')
rect2 = patches.Rectangle((lb[0],lb[1]),ub[0]-lb[0],ub[1]-lb[1],linewidth=1,edgecolor='r',facecolor='none')
rect3 = patches.Rectangle((lb[0],lb[1]),ub[0]-lb[0],ub[1]-lb[1],linewidth=1,edgecolor='r',facecolor='none')
axarr[0].contour(xx, yy, F, np.logspace(-1, 3, xn))
axarr[0].add_patch(rect1)
axarr[0].set_xlim([domxl, domxu])
axarr[0].set_ylim([domyl, domyu])
axarr[0].set_ylabel('y')
axarr[0].title.set_text('Function value')
axarr[1].contour(xx, yy, G, np.logspace(-1, 3, xn))
axarr[1].add_patch(rect2)
axarr[1].set_xlim([domxl, domxu])
axarr[1].set_ylim([domyl, domyu])
axarr[1].set_ylabel('y')
axarr[1].title.set_text('L2 norm of gradient')
axarr[2].contour(xxb, yyb, pGb, np.logspace(-1, 3, xn))
axarr[2].add_patch(rect3)
axarr[2].set_xlim([domxl, domxu])
axarr[2].set_ylim([domyl, domyu])
axarr[2].set_xlabel('x')
axarr[2].set_ylabel('y')
axarr[2].title.set_text('L2 norm of VecFischer(gradient)')

plt.show()