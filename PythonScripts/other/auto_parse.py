#!/usr/bin/env python

from builtins import range
import os
import numpy as np
import argparse
import fnmatch
# profiling settings
probtype = 'bound'
solvers = ['blmvm/bfgs','blmvm/dfp','blmvm/symbrdn','bqnkls/sr1','bqnktr/sr1']
outname = 'cg'

# configure the parser
parser = argparse.ArgumentParser()
parser.add_argument('-p', '--probtype', type=str, nargs=1, default=[probtype], help='specify the CUTEst problem set to be tested')
parser.add_argument('-s', '--solvers', type=str, nargs='+', default=solvers, help='specify the TAO algorithm/option combinations to be parsed')
parser.add_argument('-o', '--outname', type=str, nargs=1, default=[outname], help='specify a name for this comparison')
parser.add_argument('-a', '--all', default=False, action="store_true", help='force all problems (ignore excludes)')
parser.add_argument('-e', '--excludes', default=False, action="store_true", help='run excluded problems')
args = parser.parse_args()
probtype = args.probtype[0]
solvers = args.solvers  # should only need to submit the phrase bncg, to get all bncg methods, for example. bncg blmvm would give all solvers in both, hopefully...
slv_tmp = []
for solver in solvers:
    
    for walker in os.walk('%s/%s'%(probtype, solver)):
        
        if (len(walker[2]) >= 1):
            outs = fnmatch.filter(walker[2], '*.out')
            
            if (len(outs) > 2): # should only contain .out files. Might contain one other file (tao_options.options), so I check if there are more than 1        
                dir_start = len(probtype)+1
                slv_tmp.append(walker[0][dir_start::]) # appends directory name; gets rid of first two characters, which are "./"

solvers = slv_tmp
#print(slv_tmp[0:2])
#flat_list = [item for sublist in l for item in sublist]
#solvers = [solver for subsolver in l for item in slv_temp]
outname = args.outname[0]
run_all = args.all
run_excludes = args.excludes

# cost factors
fcost = 1
fgcost = 2
gcost = 2

# prepare the list of CUTEst problems to be evaluated
fname = '%s/probs.txt'%probtype
with open(fname) as f:
    problems = f.readlines()
problems = [p.strip() for p in problems]
fname = '%s/excludes.txt'%probtype
with open(fname) as f:
    excludes = f.readlines()
excludes = [p.strip() for p in excludes]

if run_all:
    final_set = problems
elif run_excludes:
    final_set = excludes
else:
    final_set = [p for p in problems if p not in excludes]

# initialize data structures for storing optimization results
iters = np.zeros((len(problems), len(solvers)))
fevals = np.zeros((len(problems), len(solvers)))
kspits = np.zeros((len(problems), len(solvers)))
cputime = np.zeros((len(problems), len(solvers)))
fails = [np.array(list(), dtype=object) for i in range(len(solvers))]

# cost factors
fcost = 1
fgcost = 2
gcost = 2

# loop over solvers
h = ''
for sidx, s in enumerate(solvers):
    h += ' %s'%s
    dirname = '%s/%s'%(probtype, s)
    if not os.path.exists(dirname):
        raise FileNotFoundError('The solver/option for %s combination does not exist!'%s)
    # loop over problems for the solver
    for pidx, p in enumerate(final_set):
        dataname = '%s/%s.out'%(dirname, p)
        with open(dataname) as f:
            results = f.readlines()
        results = [l.strip() for l in results]
        results = [_f for _f in results if _f]
        # process the results
        if 'Exit message     CONVERGENCE: GTOL TEST SATISFIED' in results:
            for item in results:
                # tally up iteration evaluations
                if item.startswith('total number of iterations='):
                    tmp = item.split(',')
                    tmp = tmp[0].split('=')
                    iters[pidx, sidx] = int(tmp[-1])
                    #print(iters[pidx, sidx])
                # tally up function evaluations
                if item.startswith('total number of function evaluations='):
                    tmp = item.split(',')
                    tmp = tmp[0].split('=')
                    fevals[pidx, sidx] += int(fcost*tmp[-1])
                if item.startswith('total number of function/gradient evaluations='):
                    tmp = item.split(',')
                    tmp = tmp[0].split('=')
                    fevals[pidx, sidx] += int(fgcost*tmp[-1])
                if item.startswith('total number of gradient evaluations='):
                    tmp = item.split(',')
                    tmp = tmp[0].split('=')
                    fevals[pidx, sidx] += int(gcost*tmp[-1])
                if item.startswith('total KSP iterations:'):
                    tmp = item.split(' ')
                    kspits[pidx, sidx] = int(tmp[-1])
                if item.startswith('CPU time (s) since USETUP'):                    
                    tmp = item.split(' ')
                    cputime[pidx, sidx] = float(tmp[-1])

        else:
            iters[pidx, sidx] = np.nan
            fevals[pidx, sidx] = np.nan
            kspits[pidx, sidx] = np.nan
            cputime[pidx, sidx] = np.nan
            fails[sidx] = np.append(fails[sidx], p)

# save iteration based performance data
fname = '%s/iters_%s.out'%(probtype,outname)
if os.path.isfile(fname):
    os.remove(fname)
np.savetxt(fname, iters, fmt='%g', header=h)

# save function/gradient based performance data
fname = '%s/fevals_%s.out'%(probtype,outname)
if os.path.isfile(fname):
    os.remove(fname)
np.savetxt(fname, fevals, fmt='%g', header=h)

# save KSP iteration based performance data
fname = '%s/kspits_%s.out'%(probtype,outname)
if os.path.isfile(fname):
    os.remove(fname)
np.savetxt(fname, kspits, fmt='%g', header=h)

# save CPU time based performance data
fname = '%s/cputime_%s.out'%(probtype,outname)
if os.path.isfile(fname):
    os.remove(fname)
np.savetxt(fname, cputime, fmt='%g', header=h)
        
# save the failed problems in a file
for i in range(len(solvers)):
    arr = solvers[i].split('/')
    solver = arr[0]
    if len(arr) > 1:
        opt = arr[1]
        fname = '%s/%s-%s_fails.out'%(probtype, solver, opt)
    else:
        fname = '%s/%s_fails.out'%(probtype, solver)
    if os.path.isfile(fname):
        os.remove(fname)
    np.savetxt(fname, fails[i], fmt='%s')
