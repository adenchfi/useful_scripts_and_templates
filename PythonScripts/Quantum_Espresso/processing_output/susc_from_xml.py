import xml.etree.ElementTree as ET
import numpy as np
import matplotlib.pyplot as plt

def fermi_dist(b, eps, f=0):
    if    (  (eps-f)*b > 8  ):
        return 0
    elif  (  (eps-f)*b < -8 ):
        return 1
    else:
        tmp = 1/(np.exp((eps - f)*b) - 1)
        return tmp


name = input("Input xml file to analyze for computing susceptibility: ")
T = float(input("Input the temperature of the system: "))
fermi = float(input("Input the fermi energy of the system: "))
interorb = input("Consider nesting between different orbitals/bands? (Y/N): ")
kb = 8.617e-5
epsilon = 5e-6
beta = 1/(kb*T)
tree = ET.parse(name)
root = tree.getroot()
# if this is an xml file produced by QE, I can get the bandstructure via:
out = root.find('output')
bs = out.find('band_structure')

# chi(q) = \sum_k \frac{f(eps_{k+q} - f(eps_{k}))}{eps_{k+q} - eps_{k}}
# should also sum over all orbitals, not just orbitals at FS
# usual approx is f(k+q) - f(k) is only nonzero around FS, as below FS both are around 1, and above, both are around 0
# however, esp. at high temps, no longer the case

ks_ens = bs.findall('ks_energies')

ks = []
chis = []
fs_chis = []  # like chi(q), except for T=epsilon, aka beta large

beta_fs = 1/(kb*epsilon)
##### TEMPORARILY HERE: replacing with k, k+q form soon?
for kp in ks_ens:
    tmp = 0
    tmp_fs = 0
    # chi(kp) = \sum_{k,n} (f(eps_{kp,n}) - f(eps_{k,n})) / (eps_{kp,n} - eps_{k})
    kpxyz = (kp.find('k_point')).text
    for k in ks_ens:
        kxyz = (k.find('k_point')).text
        if (kpxyz != kxyz):
            evs_at_kp = ((kp.find('eigenvalues')).text).split()
            evs_at_k = ((k.find('eigenvalues')).text).split()
            # technically, I want to compute this for not just same orbitals, but also between orbitals; will make this take longer though...
            if (interorb == "Y"):
                for e_kp in evs_at_kp:
                    for e_k in evs_at_k:
                        #            
                        tmp2 = (fermi_dist(beta, float(e_kp), fermi) - fermi_dist(beta, float(e_k), fermi))/(float(e_kp) - float(e_k))
                        tmp3 = (fermi_dist(beta_fs, float(e_kp), fermi) - fermi_dist(beta_fs, float(e_k), fermi))/(float(e_kp) - float(e_k))
                        if (np.isnan(tmp2)):
                            tmp2 = 1e8 # some large value
                        if (np.isnan(tmp3)):
                            tmp3 = 1e8
                        tmp += tmp2
                        tmp_fs += tmp3
            elif (interorb == "N"):
                for e_kp, e_k in zip(evs_at_kp, evs_at_k):
                    tmp2 = (fermi_dist(beta, float(e_kp), fermi) - fermi_dist(beta, float(e_k), fermi))/(float(e_kp) - float(e_k))
                    tmp3 = (fermi_dist(beta_fs, float(e_kp), fermi) - fermi_dist(beta_fs, float(e_k), fermi))/(float(e_kp) - float(e_k))
                    if (np.isnan(tmp2)):
                        tmp2 = 1e8 # some large value
                    if (np.isnan(tmp3)):
                        tmp3 = 1e8
                        
                    tmp += tmp2
                    tmp_fs += tmp3
            else:
                print("Didn't input Y or N!")
                exit
        # else: tmp += 0; trying to avoid 0/0
                
    ks.append((kp.find('k_point')).text)
    chis.append(tmp)
    fs_chis.append(tmp_fs)
##### END temporary form
    
    
chis = chis/(np.max(chis)) # normalize them
fs_chis = fs_chis/(np.max(fs_chis))
for k, chi, fs_chi in zip(ks, chis, fs_chis):
    if chi > 0.0005:
        print("K point for large chi\t\t\t", k)
        print("Chi (normalized)\t\t\t", chi)
    if fs_chi > 0.0005:
        print("K point for large FS nesting\t\t", k)
        print("FS nesting amplitude\t\t\t", fs_chi)
        

plt.plot(chis)
plt.plot(fs_chis)
plt.show()
#for ks_en in bs.findall('ks_energies'):
#    kpt = ks_en.find('k_point')
#    kptxyz = kpt.text # should print out xyz coords of kpt in I guess some coord system
#    egnvals = ks_en.find('eigenvalues') # should have N elements in the text, where N = size = nbnd
#    evs_at_kpt = (egnvals.text).split() # for LaTe3 should have 59 for each kpt, for example
    
