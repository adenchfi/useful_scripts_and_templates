import numpy as np
import matplotlib.pyplot as plt

x = [574.3, 802.7, 61, 358.1, 62.4, 460.1]
y = [835, 1173, 136, 511, 88, 662] # 10, not 9, so the fit isn't perfect

fit = np.polyfit(x,y,1)
fit_fn = np.poly1d(fit)
# fit_fn is now a function which takes in x and returns an estimate for y

plt.plot(x,y, 'yo', x, fit_fn(x), '--k')
plt.xlabel("Channel Number")
plt.ylabel("Energy (keV)")
plt.show()
