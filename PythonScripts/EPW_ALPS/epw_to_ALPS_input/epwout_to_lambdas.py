### this script converts the output of a modified EPW calculation into the electron-phonon couplings lambda_{i, n, nu} for site i, electron band/orbital n, phonon branch nu. This is for defining a model Hamiltonian for the ALPS CT-HYB code, but for that we will need the phonon frequencies also; that will be a separate script/problem for now.

import shutil, sys, os, fileinput
import numpy as np

imag = 1j

def find_atoms(filepwout):
    # want to find the real-space positions of the atoms in crystal axes (e.g. in units of a,b,c)
    f = open(filepwout, 'r')
    lines = f.readlines()
    natoms = 1
    atompos = []
    for idx, line in enumerate(lines):
        if ("number of atoms/cell" in line):
            natoms = int(line.split()[-1])
        if ("positions (a_0 units)" in line):
            for i in range(natoms):
                site = lines[idx + i + 1]
                site = site.split()
                posx = float(site[-4])
                posy = float(site[-3])
                posz = float(site[-2])
                atompos.append((posx,posy,posz))
    return (natoms, atompos)
def find_qmesh(filepwout):
    f = open(filepwout, 'r')
    lines = f.readlines()
    startidx = 0
    kpts = 0
    for idx, line in enumerate(lines):
        if ("number of k points=" in line):  # for now assuming fine k mesh is same as fine q mesh
            words = line.split()
            print(words)
            kpts = int(words[4]) # should be fifth one
            startidx = idx + 2
            
    klines = lines[startidx:startidx+kpts:1] # +1 because indexing is weird
    print("Number of lines used: ", np.shape(klines))
    print("number of kpts detected: ", kpts)
    ks = []
    for kl in klines:
        words = kl.split()
        kx = float(words[4])
        ky = float(words[5])
        kz = words[6] # has a ), at the end
        kz = float(kz[0:-3]) # should work
    
        ks.append((kx, ky, kz))
    # assuming fine kmesh is same as fine qmesh
    qs = ks
    qsnp = np.array(qs)
    
    np.savetxt("qs.dat", qsnp)
    return qsnp
def construct_realspace_omegas():
    #filqs = input("Enter the filename of the file that contains the fine q-mesh for which omega_{q nu} was made: ")
    #filqs = "qs.dat"
    epwout = input("Enter the name of the epw output file (e.g. epw.out): ")
    qs = find_qmesh(epwout)
    # above should have 512 rows
    filwqs = input("Enter the filename of the linewidth.phself file EPW produces (e.g. linewidth.phself): ")
    qpts, modes, freqmevs, _ = np.loadtxt(filwqs, skiprows=2, unpack=True)
    nqpts = int(max(qpts))
    nmodes = int(max(modes))
    qpts = qpts.astype(int)
    modes = modes.astype(int)
    wqnus=np.zeros((nqpts, nmodes))
    for q in range(nqpts):
        for nu in range(nmodes):
            wqnus[q,nu] = freqmevs[(q)*nmodes + nu]


    # we now have matrix omega_{q nu}
    # would like to construct matrix omega_{j nu} where {j} is a location of an atom

    # so we need atomic locations
    # we want this to represent the energy cost of moving atom at location (j) along direction (nu)?
      # but say one atom is at location j=(0,0,0). There would be no dot product of it with anything. So e^(i j \dot q) = 1 for that atom?
    natoms, atom_locs = find_atoms(epwout)
    
    omega_jnus = np.zeros((natoms, nmodes))
    print("qs: ", qs)
    w = open("omega_jnus.dat", 'w+') # open a file if nonexistent
    w.write("j   nu   omega_{j nu}\n")
    for j in range(natoms):
        print(atom_locs[j])
        for nu in range(nmodes):
            omega_tmp = 0
            for q in range(nqpts):
                exp_arg = (atom_locs[j][0])*qs[q][0] + (atom_locs[j][1])*qs[q][1] + (atom_locs[j][2])*qs[q][2]
                exp_tmp = np.exp(-imag*exp_arg)
                omega_tmp += exp_tmp*wqnus[q,nu]
            
            omega_jnus[j,nu] = omega_tmp
            print("Imaginary part of omega_jnu: ", np.imag(omega_tmp))
            msg = str(j) + "  " + str(nu) + "  " + str(omega_jnus[j,nu]) + "\n"
            w.write(msg)

    # the above may be right up to a constant multiple?
    #omegas = np.array(omega_jnus)


    
## takes in filename to read as first input; outputs a file with given name
def construct_lambdas():
    filename = input("Enter the filename with el-ph couplings: ")
    fileout = "lambda_i_n_nu.dat"

    f=open(filename, 'r')
    lines = f.readlines()

    # we will construct the lambda=g_{m=n nu R_e=R_p} matrix
    # it will not be the most efficient, but this won't be a huge file
    fw=open(fileout, 'w+')
    fw.write('r_x, r_y, r_z, n, nu, g\n')
    
    for line in lines:
        words  = line.split()
        if (10 == len(words)):
            # these are the correct lines, hopefully
            m = words[0]
            n = words[1]
            nu = words[2]
            R_e = words[3:6:1] # for some reason python doesn't include the last element in this indexing; this is 3-6 as written, but really 3-5
            R_p = words[6:9:1]
            g = words[9]
            if (m == n and R_e == R_p):
                str = R_e[0] + ' ' + R_e[1] + ' ' + R_e[2] + ' '
                str += n + '  '
                str += nu + '  '
                str += g + '\n'
                fw.write(str)
